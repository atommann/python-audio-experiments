import numpy as np
from scipy.io import wavfile

rate, data = wavfile.read('demo.wav')
#rate, data = scipy.io.wavfile.read('left-right.wav')

print(len(data.shape))# 1 if mono,    2 if stereo
# if stereo, x is a 2-dimensional array, so we can access both channels with :
print(data[:,0])
print(data[:,1])

newdata = data[:,0] - data[:,1]

wavfile.write("left.wav", rate, data[:,0])
wavfile.write("right.wav", rate, data[:,1])

wavfile.write("fft.wav", rate, newdata)
