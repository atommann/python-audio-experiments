# https://stackoverflow.com/questions/23730796/using-pydub-to-chop-up-a-long-audio-file
from pydub import AudioSegment
from pydub.silence import split_on_silence

sound = AudioSegment.from_wav("fft.wav")
# the unit is milliseconds
print "length:", len(sound)

chunks = split_on_silence(sound, 
    # must be silent for at least half a second
    min_silence_len=500,

    # consider it silent if quieter than -16 dBFS
    #silence_thresh=-16
)

for i, chunk in enumerate(chunks):
    chunk.export("chunk{0}.wav".format(i), format="wav")

start_msec = 1500
end_msec = 1600
clip_data = sound[start_msec:end_msec]
# the unit is milliseconds
print "length:", len(clip_data)
clip_data.export("clip.wav", format="wav")
